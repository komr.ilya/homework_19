#include <iostream>

class Animal
{
public:
    Animal() {}

    virtual void Voice()
    {
        std::cout << "Aaaa!" << std::endl;
    }
};

class Dog : public Animal
{
public:
    Dog() {}

    void Voice() override
    {
        std::cout << "Woof!" << std::endl;
    }
};

class Cat : public Animal
{
public:
    Cat() {}

    void Voice() override
    {
        std::cout << "Mew!" << std::endl;
    }
};

class Duck : public Animal
{
public:
    Duck() {}

    void Voice() override
    {
        std::cout << "Ga-Ga!" << std::endl;
    }
};


int main()
{

    Animal* anAnimal = new Animal;
    Animal* aDog = new Dog;
    Animal* aCat = new Cat;
    Animal* aDuck = new Duck;

    Animal* animals[4] = {anAnimal, aDog, aCat, aDuck};
    
    for (int i = 0; i < 4; i++)
    {
        animals[i]->Voice();
    }
    


}